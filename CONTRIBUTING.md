## Contributing to Glasnost

Hey there! Thanks for being interested in contributing :-)

### Issues / new features / feedback

Would you like to see a new feature on Glasnost? Or maybe you found a bug? Or you want to just say hello? Feel free 
to write over our [issue tracker][issues].

### Developing

Glasnost is written in Javascript on top of [React Native][rn]. 

To develop the application on your PC you need to set up the environment as explained in the [official guide][rn-start].

After that, clone this repository, and install all the dependencies:
```shell script
yarn
```

From now on, when you want to start the application, you just need to choose your target and leave all the black magic 
to yarn:

```shell script
yarn android ( yarn ios )
yarn start
```

[issues]: https://gitlab.com/puskin/Glasnost/issues
[rn]: https://facebook.github.io/react-native/
[rn-start]: https://facebook.github.io/react-native/docs/getting-started
