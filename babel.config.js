module.exports = {
  presets: ["module:metro-react-native-babel-preset"],
  plugins: [
    ["@babel/transform-runtime"],
    ["@babel/plugin-syntax-throw-expressions"],
    [
      "module-resolver",
      {root: ["./src/node_modules"]}
    ]
  ]
};
