import _ from 'lodash';
import {createSelector} from 'reselect';

const getMergeRequests = state => _.get(state, ['mergeRequests', 'mergeRequests']);
export const getMergedRequests = createSelector(getMergeRequests, mergeRequests => _.filter(mergeRequests, {state: 'merged'}));
export const getOpenMergeRequests = createSelector(getMergeRequests, mergeRequests => _.filter(mergeRequests, {state: 'opened'}));
export const getClosedMergeRequests = createSelector(getMergeRequests, mergeRequests => _.filter(mergeRequests, {state: 'closed'}));
