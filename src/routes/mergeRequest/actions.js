import _ from 'lodash';

import {del, get, post, put} from 'api';

import {getRepository} from '../repository/selectors';
import {getMergeRequest} from './selectors';
import {requestJobsForPipeline} from '../pipelines/actions';
import {getBranchName} from '../branches/selectors';
import {requestRepository, requestRepositoryApiCall} from '../repository/actions';

export function requestDiscussions() {
    return async (dispatch, getState) => {
        const mr = getMergeRequest(getState());

        if (_.isEmpty(mr)) {
            return;
        }

        const {data} = await get({
            path: `projects/${mr.project_id}/merge_requests/${mr.iid}/discussions`,
            additionalParams: {per_page: 100}
        });

        if (data) {
            dispatch({type: 'DISCUSSIONS_RECEIVED', data});
        }
    };
}

export function requestMrDiffs() {
    return async (dispatch, getState) => {
        const state = getState();

        const mr = getMergeRequest(state);

        if (_.isEmpty(mr)) {
            return;
        }

        const repo = (await requestRepositoryApiCall(mr.project_id)) || {};

        const branch = getBranchName(state) || _.get(repo, 'data.default_branch');

        const {data} = await get({
            path: `projects/${mr.project_id}/repository/compare`,
            additionalParams: {
                from: branch,
                to: mr.source_branch,
                per_page: 100
            },
            pagination: false
        });

        if (data) {
            dispatch({type: 'DIFFS_RECEIVED', data});
        }
    };
}

export function requestThumbs() {
    return async (dispatch, getState) => {
        const mr = getMergeRequest(getState());

        if (_.isEmpty(mr)) {
            return;
        }

        const {data} = await get({path: `projects/${mr.project_id}/merge_requests/${mr.iid}/award_emoji`});

        if (data) {
            dispatch({type: 'THUMBS_RECEIVED', data});
        }
    };
}

export function requestMergeRequestApiCall(projectId, iid) {
    return get({path: `projects/${projectId}/merge_requests/${iid}`});
}

export function requestMergeRequest(iid, projectId, forceReload = false) {
    return async (dispatch, getState) => {
        const repo = getRepository(getState());
        const repoId = projectId || _.get(repo, 'id');

        if (!repoId) {
            return;
        }

        const {data} = await requestMergeRequestApiCall(repoId, iid);

        if (data) {
            dispatch({type: 'MERGE_REQUEST_RECEIVED', data});
            const pipelineId = _.get(data, 'pipeline.id');
            dispatch(requestThumbs());
            dispatch(requestJobsForPipeline(repoId, pipelineId, forceReload));
            dispatch(requestDiscussions());
            dispatch(requestRepository(repoId));
        }
    };
}

export function editMergeRequest(changes) {
    return async (dispatch, getState) => {
        const mr = getMergeRequest(getState());

        if (_.isEmpty(mr)) {
            return;
        }

        const newMr = await put({path: `projects/${mr.project_id}/merge_requests/${mr.iid}`, body: changes});

        dispatch({type: 'MERGE_REQUEST_RECEIVED', mergeRequest: newMr});
    };
}

export function addThumbs(direction) {
    return async (dispatch, getState) => {
        const mr = getMergeRequest(getState());

        if (_.isEmpty(mr)) {
            return;
        }

        await post({
            path: `projects/${mr.project_id}/merge_requests/${mr.iid}/award_emoji`,
            data: {name: direction}
        });

        dispatch(requestMergeRequest(mr.iid));
    };
}

export function removeThumbs(id) {
    return async (dispatch, getState) => {
        const mr = getMergeRequest(getState());

        if (_.isEmpty(mr)) {
            return;
        }

        await del({path: `projects/${mr.project_id}/merge_requests/${mr.iid}/award_emoji/${id}`});

        dispatch(requestMergeRequest(mr.iid));
    };
}

export function commentMergeRequest(body) {
    return async (dispatch, getState) => {
        const mr = getMergeRequest(getState());

        if (_.isEmpty(mr)) {
            return;
        }

        await post({path: `projects/${mr.project_id}/merge_requests/${mr.iid}/discussions`, body});

        dispatch(requestDiscussions());
    };
}
