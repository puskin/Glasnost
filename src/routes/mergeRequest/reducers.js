import {registerReducer} from 'store';

const mergeRequest = {
    MERGE_REQUEST_RECEIVED: (state, {data}) => ({...state, mergeRequest: data}),
    DISCUSSIONS_RECEIVED: (state, {data}) => ({...state, discussions: data}),
    THUMBS_RECEIVED: (state, {data}) => ({...state, thumbs: data}),
    DIFFS_RECEIVED: (state, {data}) => ({...state, diffs: data})
};

registerReducer('mergeRequest', mergeRequest);
