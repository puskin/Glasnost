import {registerReducer} from 'store';
import _ from 'lodash';

const wikis = {
    WIKIS_RECEIVED: (state, {data}) => ({...state, wikis: _.uniqBy([...(state.wikis || []), ...data], 'slug')}),
    SET_WIKIS_PAGINATION: (state, {pagination}) => ({...state, pagination}),
    CLEAR_WIKIS: state => ({...state, wikis: []})
};

registerReducer('wikis', wikis);
