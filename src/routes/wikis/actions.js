import _ from 'lodash';

import {get} from 'api';
import {getPagination, setPagination} from 'utils';

import {getRepository} from '../repository/selectors';

export function clearWikis() {
    return {type: 'CLEAR_WIKIS'};
}

export function requestWikis(page0) {
    return async (dispatch, getState) => {
        const repo = getRepository(getState());

        if (_.isEmpty(repo)) {
            return;
        }

        const {pageToLoad = 0} = getPagination(getState(), 'commits');

        const {data, headers} = await get({
            page: page0 ? 0 : pageToLoad,
            path: `projects/${repo.id}/wikis`,
            cached: !page0
        });

        if (data) {
            dispatch({type: 'WIKIS_RECEIVED', data});
            dispatch(setPagination('wikis', headers));
        }
    };
}
