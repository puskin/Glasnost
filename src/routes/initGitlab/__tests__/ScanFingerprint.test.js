import React from 'react';
import {Provider} from 'react-redux';

import {getRegisteredRoutes} from 'router';

describe('ScanFingerprint', () => {
    let ScanFingerprint;
    let store;
    let fingerprint;

    beforeAll(() => {
        const {setStore} = require('api');
        const {getStore} = require('store');

        require('../index');
        setStore(getStore());
    });

    beforeEach(() => {
        jest.clearAllMocks();
        fingerprint = require('react-native-fingerprint-scanner/src');
        ScanFingerprint = getRegisteredRoutes().ScanFingerprint;

        const {getStore} = require('store');
        store = getStore();
    });

    it('should render correctly the ScanFingerprint page', async () => {
        const component = await asyncCreate(<Provider store={store}><ScanFingerprint/></Provider>);

        expect(component.toJSON()).toMatchSnapshot();
    });

    it('should call the functions when unmounting', async () => {
        fingerprint.release = jest.fn();

        const component = mount(<Provider store={store}><ScanFingerprint/></Provider>);

        component.unmount();
        expect(fingerprint.release).toHaveBeenCalled();
    });

    xit('should authenticate successfully', async () => {
        fingerprint.authenticate = jest.fn().mockResolvedValue(Promise.resolve({}));

        const component = mount(<Provider store={store}><ScanFingerprint/></Provider>);

        expect(component.html()).toMatchSnapshot();
    });

    xit('should not authenticate successfully', async () => {
        fingerprint.authenticate = jest.fn().mockRejectedValue(Promise.reject({}));

        const component = mount(<Provider store={store}><ScanFingerprint/></Provider>);

        expect(component.html()).toMatchSnapshot();
    });
});
