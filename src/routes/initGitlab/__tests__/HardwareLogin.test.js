import React from 'react';
import {Provider} from 'react-redux';

import {setCurrentUrl} from 'utils';
import {getRegisteredRoutes, getRouteName} from 'router';

describe('HardwareLogin', () => {
    let HardwareLogin;
    let store;
    let fingerprint;

    beforeAll(() => {
        const {setStore} = require('api');
        const {getStore} = require('store');

        require('../index');
        setStore(getStore());
    });

    beforeEach(() => {
        jest.clearAllMocks();
        fingerprint = require('react-native-fingerprint-scanner/src');
        fingerprint.isSensorAvailable = () => 'Fingerprint';

        HardwareLogin = getRegisteredRoutes().HardwareLogin;

        const {getStore} = require('store');
        store = getStore();
    });

    it('should not render because the phone has no fingerprint sensor', async () => {
        fingerprint.isSensorAvailable = () => {
            throw 'nope';
        };
        const component = await asyncCreate(<Provider store={store}><HardwareLogin/></Provider>);

        expect(component.toJSON()).toMatchSnapshot();
    });

    it('should render correctly the HardwareLogin page', async () => {
        const component = await asyncCreate(<Provider store={store}><HardwareLogin/></Provider>);

        expect(component.toJSON()).toMatchSnapshot();
    });

    it('should navigate to Repositories when clicking on the fingerprint button', async () => {
        store.dispatch(setCurrentUrl('https://someGitlabUrl.com'));
        const component = mount(<Provider store={store}><HardwareLogin/></Provider>);
        await waitForAsync();
        component.update();

        onGet({url: /user*/, data: {}});

        const MainButton = component.find('MainButton').at(1);
        MainButton.props().onPress();
        await waitForAsync();
        component.update();

        expect(getRouteName(store.getState())).toEqual('Repositories');
    });

    it('should navigate to Repositories when clicking on the skip button', async () => {
        store.dispatch(setCurrentUrl('https://someGitlabUrl.com'));
        const component = mount(<Provider store={store}><HardwareLogin/></Provider>);
        await waitForAsync();
        component.update();

        onGet({url: /user*/, data: {}});

        const MainButton = component.find('MainButton').at(0);
        MainButton.props().onPress();
        await waitForAsync();
        component.update();

        expect(getRouteName(store.getState())).toEqual('Repositories');
    });
});
