import _ from 'lodash';
import {createSelector} from 'reselect';

const getIssues = state => _.get(state, 'issues.issues');
export const getOpenIssues = createSelector(getIssues, issues => _.filter(issues, {state: 'opened'}));
export const getClosedIssues = createSelector(getIssues, issues => _.filter(issues, {state: 'closed'}));
