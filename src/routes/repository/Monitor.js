import React from 'react';
import moment from 'moment';
import _ from 'lodash';
import {View} from 'react-native';
import {connect} from 'react-redux';

import {Card, colors, FormattedText} from 'elements';

import {getClosedMergeRequests, getMergedRequests, getOpenMergeRequests} from '../mergeRequests/selectors';
import {getClosedIssues, getOpenIssues} from '../issues/selectors';

function MonitorComponent({openMrs, closedMrs, mergedMrs, openIssues, closedIssues}) {
    if (!_.size(openMrs) && !_.size(closedMrs) && !_.size(openIssues) && !_.size(closedIssues)) {
        return null;
    }

    const now = moment();
    const monthOpenMrs = _.filter(openMrs, mr => moment(mr.created_at).diff(now, 'days') <= 31);
    const monthClosedMrs = _.filter(closedMrs, mr => moment(mr.created_at).diff(now, 'days') <= 31);
    const monthMergedMrs = _.filter(mergedMrs, mr => moment(mr.created_at).diff(now, 'days') <= 31);
    const monthOpenIssues = _.filter(openIssues, issue => moment(issue.created_at).diff(now, 'days') <= 31);
    const monthClosedIssues = _.filter(closedIssues, issue => moment(issue.created_at).diff(now, 'days') <= 31);

    const hasMrs = _.size(monthOpenMrs) || _.size(monthClosedMrs) || _.size(mergedMrs);
    const hasIssues = _.size(monthOpenIssues) || _.size(monthClosedIssues);

    return (
        <Card title={'Monitor'} icon={{name: 'monitor'}}>
            {!!hasMrs && <MonitorBar stats={{'Open MRs': _.size(monthOpenMrs), 'Closed MRs': _.size(monthClosedMrs), 'Merged MRs': _.size(monthMergedMrs)}}/>}
            {!!hasIssues
            && <MonitorBar stats={{'Open Issues': _.size(monthOpenIssues), 'Closed Issues': _.size(monthClosedIssues)}}/>}
        </Card>
    );
}

function MonitorBar({stats}) {
    const bgColor = [colors.gray0, colors.gray1, colors.lighGray];
    const total = _.reduce(stats, (res, value) => res + value, 0);
    const percentages = total ? _.map(stats, prop => parseInt(prop / total * 100)) : new Array(_.size(stats)).fill(0);

    return (
        <View style={{flex: 1, flexDirection: 'column', marginTop: 10}}>
            {_.map(percentages, (perc, index) => (
                <View key={index} style={{flexDirection: 'row', alignItems: 'center'}}>
                    <View style={{flex: 0.9, flexDirection: 'row'}}>
                        <FormattedText key={Object.keys(stats)[index]}>{Object.keys(stats)[index]}</FormattedText>
                        <View style={{flex: 1}}>
                            <FormattedText style={{textAlign: 'right'}}>{`${perc}%`}</FormattedText>
                        </View>
                    </View>
                    <View style={{flex: 0.9, marginLeft: 10}}>
                        <View key={index} style={{backgroundColor: bgColor[index], width: `${perc}%`, height: 15}}/>
                    </View>
                </View>
            ))}
        </View>
    );
}

export const Monitor = connect(state => ({
    openMrs: getOpenMergeRequests(state),
    closedMrs: getClosedMergeRequests(state),
    mergedMrs: getMergedRequests(state),
    openIssues: getOpenIssues(state),
    closedIssues: getClosedIssues(state)
}))(MonitorComponent);
