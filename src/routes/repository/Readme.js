import React, {Component} from 'react';
import {connect} from 'react-redux';

import {Markdown} from 'elements';

import {clearReadme} from './actions';
import {getReadme} from './selectors';

class ReadmeComponent extends Component {
    componentWillUnmount() {
        this.props.clearReadme();
    }

    render() {
        if (!this.props.readme) {
            return null;
        }

        return <Markdown text={this.props.readme}/>;
    }
}

export const Readme = connect(state => ({readme: getReadme(state)}), {clearReadme})(ReadmeComponent);
