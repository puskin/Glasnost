import {getRegisteredRoutes} from 'router';
import {getReducers} from 'store';

import * as index from '../index';

describe('Branches index', () => {
    it('should export Branches and reducers', () => {
        expect(JSON.stringify(index)).toEqual('{}');
        expect(getRegisteredRoutes().Branches).toBeDefined();
        expect(getReducers().branches).toBeDefined();
    });
});
