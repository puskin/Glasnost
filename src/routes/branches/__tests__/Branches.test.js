import React from 'react';
import {Provider} from 'react-redux';

import {getRegisteredRoutes, getRouteName} from 'router';
import {getHeaderLeftButton} from 'headerFooter/selectors';

describe('Branches', () => {
    let Branches;
    let store;

    beforeAll(() => {
        const {setStore} = require('api');
        const {getStore} = require('store');

        require('../index');
        require('../../repository/reducers');
        setStore(getStore());
    });

    beforeEach(() => {
        Branches = getRegisteredRoutes().Branches;

        const {getStore} = require('store');
        store = getStore();
    });

    it('renders correctly the empty list', async () => {
        const component = await asyncCreate(<Provider store={store}><Branches/></Provider>);

        expect(component.toJSON()).toMatchSnapshot();
    });

    it('it should navigate to the Repository page when clicking on the back button', async () => {
        mount(<Provider store={store}><Branches/></Provider>);
        const backButton = getHeaderLeftButton(store.getState());

        backButton.props.onPress();
        expect(getRouteName(store.getState())).toEqual('Repository');
    });

    it('should not render the list if data is empty', async () => {
        store.dispatch({type: 'REPOSITORY_RECEIVED', data: {id: 1}});
        onGet({url: /branches*/, data: null});

        const component = await asyncCreate(<Provider store={store}><Branches/></Provider>);

        expect(component.toJSON()).toMatchSnapshot();
    });

    it('renders the list with some branch', async () => {
        store.dispatch({type: 'REPOSITORY_RECEIVED', data: {id: 1}});
        onGet({url: /branches*/, data: [{id: 1, name: 'master'}, {id: 2, name: 'someOtherBranch'}]});

        const component = await asyncCreate(<Provider store={store}><Branches/></Provider>);

        expect(component.toJSON()).toMatchSnapshot();
    });

    it('should navigate to the Repository page when clicking on a branch', async () => {
        store.dispatch({type: 'REPOSITORY_RECEIVED', data: {id: 1}});
        onGet({url: /branches*/, data: [{id: 1, name: 'master'}, {id: 2, name: 'someOtherBranch'}]});

        const component = mount(<Provider store={store}><Branches/></Provider>);
        component.find('SingleBranch').at(0).find('TouchableOpacity').props()
            .onPress();

        expect(getRouteName(store.getState())).toEqual('Repository');
    });
});
