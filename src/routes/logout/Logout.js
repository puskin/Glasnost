import React from 'react';
import {connect} from 'react-redux';

import {AppContainer, FormattedText, MainButton} from 'elements';
import {getGitlabUrl, logout} from 'utils';
import {registerRoute} from 'router';

function LogoutComponent({logout, url}) {
    return (
        <AppContainer style={{paddingHorizontal: 20}}>
            <FormattedText style={{fontSize: 20, marginVertical: 20}}>
                {`Are you sure you really want to logout from ${url} ?`}
            </FormattedText>
            <MainButton text={'Logout'} onPress={() => logout(url)}/>
        </AppContainer>
    );
}

const Logout = connect(state => ({url: getGitlabUrl(state)}), {logout})(LogoutComponent);
registerRoute({Logout});
