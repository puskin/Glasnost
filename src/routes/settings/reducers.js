import {registerReducer} from 'store';

const settings = {
    SET_THEME: (state, {isDark}) => ({...state, darkTheme: isDark === 'true'}),
    SET_LITE_EXPERIENCE: (state, {isLite}) => ({...state, liteExperience: isLite === 'true'}),
    SET_FINGERPRINT: (state, {isSet}) => ({...state, fingerprint: isSet === 'true'})
};

registerReducer('settings', settings);
