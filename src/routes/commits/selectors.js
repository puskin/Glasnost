import _ from 'lodash';
import {createSelector} from 'reselect';

const getCommitsSelector = state => _.get(state, 'commits');
export const getCommits = createSelector(getCommitsSelector, commits => _.get(commits, 'commits'));
export const getCommitDiffs = createSelector(getCommitsSelector, commits => _.get(commits, 'diffs'));
