import React from 'react';
import {Provider} from 'react-redux';
import _ from 'lodash';

import {getRegisteredRoutes, getRouteName, navigate} from 'router';
import {getHeaderLeftButton, getHeaderRightButton} from 'headerFooter/selectors';

import {getCommits} from '../selectors';

const fakeCommits = [
    {id: 1, created_at: '2019-05-11T14:30:23.406Z', message: 'someMessage1', committer_name: 'committerName1'},
    {id: 2, created_at: '2019-05-11T14:30:23.406Z', message: 'someMessage2', committer_name: 'committerName2'}
];

const otherCommit = [
    {id: 3, created_at: '2019-05-11T14:30:23.406Z', message: 'someMessage3', committer_name: 'committerName3'}
];

describe('Commits', () => {
    let Commits;
    let store;

    beforeAll(() => {
        const {setStore} = require('api');
        const {getStore} = require('store');

        require('../index');
        require('../../repository/reducers');
        setStore(getStore());
    });

    beforeEach(() => {
        Commits = getRegisteredRoutes().Commits;

        const {getStore} = require('store');
        store = getStore();

        // populate navigation's history
        store.dispatch(navigate('Repository'));
        store.dispatch(navigate('Glasnost'));
    });

    it('renders correctly the empty list with no footer', async () => {
        const {navigate} = require('router');
        store.dispatch(navigate('MergeRequest'));
        const component = await asyncCreate(<Provider store={store}><Commits/></Provider>);

        expect(component.toJSON()).toMatchSnapshot();
    });

    it('it should navigate to the Repository page when clicking on the back button', async () => {
        mount(<Provider store={store}><Commits/></Provider>);
        const backButton = getHeaderLeftButton(store.getState());

        backButton.props.onPress();
        expect(getRouteName(store.getState())).toEqual('Repository');
    });

    it('should not render the list if data is empty', async () => {
        store.dispatch({type: 'REPOSITORY_RECEIVED', data: {id: 1}});
        onGet({url: /commits*/, data: null});

        const component = await asyncCreate(<Provider store={store}><Commits/></Provider>);

        expect(component.toJSON()).toMatchSnapshot();
    });

    it('renders the list with some commit', async () => {
        store.dispatch({type: 'REPOSITORY_RECEIVED', data: {id: 1}});
        onGet({url: /commits*/, data: fakeCommits});

        const component = await asyncCreate(<Provider store={store}><Commits/></Provider>);

        expect(component.toJSON()).toMatchSnapshot();
    });

    it('should ask for more commits when clicking on the button', async () => {
        store.dispatch({type: 'REPOSITORY_RECEIVED', data: {id: 1}});
        onGet({url: /commits*/, data: fakeCommits});

        const component = mount(<Provider store={store}><Commits/></Provider>);
        await waitForAsync();

        onGet({url: /commits*/, data: otherCommit});
        const LoadMore = component.find('LoadMoreComponent');
        LoadMore.props().onPress();
        await waitForAsync();
        component.update();

        expect(component.html()).toMatchSnapshot();
    });

    it('should navigate to the CommitDiff page when clicking on a commit', async () => {
        store.dispatch({type: 'REPOSITORY_RECEIVED', data: {id: 1}});
        onGet({url: /commits*/, data: fakeCommits});

        const component = mount(<Provider store={store} commits={fakeCommits}><Commits/></Provider>);
        component.find('SingleCommit').at(0).find('TouchableOpacity').at(0)
            .props()
            .onPress();

        expect(getRouteName(store.getState())).toEqual('CommitDiff');
    });

    it('should toggle the search bar', async () => {
        store.dispatch({type: 'REPOSITORY_RECEIVED', data: {id: 1}});
        onGet({url: /commits*/, data: fakeCommits});

        const component = mount(<Provider store={store}><Commits/></Provider>);
        await waitForAsync();
        const searchButton = getHeaderRightButton(store.getState());
        searchButton.props.onPress();
        component.update();

        expect(component.find('TextInput').at(1).html()).toMatchSnapshot();
    });

    it('should toggle the search bar and filter the list', async () => {
        store.dispatch({type: 'REPOSITORY_RECEIVED', data: {id: 1}});
        onGet({url: /commits*/, data: fakeCommits});

        const component = mount(<Provider store={store}><Commits/></Provider>);
        await waitForAsync();
        const searchButton = getHeaderRightButton(store.getState());
        searchButton.props.onPress();
        component.update();

        const TextInput = component.find('TextInput').at(1);
        TextInput.props().onChangeText('someMessage2');
        TextInput.props().value = 'someMessage2';
        component.update();
        const SingleCommit = component.find('SingleCommit');

        expect(TextInput.html()).toMatchSnapshot();
        expect(SingleCommit.html()).toMatchSnapshot();
    });

    it('should call the functions on unmount', async () => {
        store.dispatch({type: 'REPOSITORY_RECEIVED', data: {id: 1}});
        onGet({url: /commits*/, data: fakeCommits});

        const component = mount(<Provider store={store}><Commits/></Provider>);

        component.unmount();

        expect(_.isEmpty(getCommits(store.getState()))).toBeTruthy();
    });

    it('should refresh the list when scrolling down', async () => {
        store.dispatch({type: 'REPOSITORY_RECEIVED', data: {id: 1}});
        onGet({url: /commits*/, data: fakeCommits});

        const component = mount(<Provider store={store}><Commits/></Provider>);
        await waitForAsync();

        const ScrollableAppContainer = component.find('ScrollableAppContainerComponent');
        ScrollableAppContainer.props().refresh();
        await waitForAsync();
        component.update();

        expect(component.html()).toMatchSnapshot();
    });
});
