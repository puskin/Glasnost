import React from 'react';
import {Provider} from 'react-redux';
import {getRegisteredRoutes, getRouteName} from 'router';
import {getFooter, getHeaderRightButton} from 'headerFooter/selectors';

const fakeIssue = {
    id: 1,
    description: 'someDescription',
    web_url: 'someUrl',
    created_at: '2019-05-11T14:30:23.406Z',
    updated_at: '2019-05-11T14:30:23.406Z',
    milestone: {title: 'someTitle', due_date: '2019-05-11T14:30:23.406Z'},
    assignees: []
};

const fakeDiscussions = [
    {id: 1, notes: [{system: false, resolved: false, author: {username: 'someUsername'}, body: 'someBody'}]},
    {id: 2, notes: [{system: false, resolved: true, author: {username: 'someUsername2'}, body: 'someBody2'}]},
    {id: 3, notes: [{system: true, resolved: true, author: {username: 'someUsername3'}, body: 'someBody3'}]}
];

describe('Issue', () => {
    let Issue;
    let store;

    beforeAll(() => {
        const {setStore} = require('api');
        const {getStore} = require('store');

        require('../index');
        require('../../repository/reducers');
        require('../../profile/reducers');
        setStore(getStore());
    });

    beforeEach(() => {
        Issue = getRegisteredRoutes().Issue;

        const {getStore} = require('store');
        store = getStore();
    });

    it('renders correctly the empty list', async () => {
        const component = await asyncCreate(<Provider store={store}><Issue/></Provider>);

        expect(component.toJSON()).toMatchSnapshot();
    });

    it('should not render the list if data is empty', async () => {
        store.dispatch({type: 'REPOSITORY_RECEIVED', data: {id: 1}});
        onGet({url: /issues*/, data: null});

        const component = await asyncCreate(<Provider store={store}><Issue/></Provider>);

        expect(component.toJSON()).toMatchSnapshot();
    });

    it('renders the issue', async () => {
        store.dispatch({type: 'REPOSITORY_RECEIVED', data: {id: 1}});
        onGet({url: /issues*/, data: fakeIssue});

        const component = await asyncCreate(<Provider store={store}><Issue/></Provider>);

        expect(component.toJSON()).toMatchSnapshot();
    });

    it('renders an open issue', async () => {
        store.dispatch({type: 'REPOSITORY_RECEIVED', data: {id: 1}});
        onGet({url: /issues*/, data: {...fakeIssue, state: 'opened'}});

        const component = await asyncCreate(<Provider store={store}><Issue/></Provider>);

        expect(component.toJSON()).toMatchSnapshot();
    });

    it('renders the issue with no milestone', async () => {
        store.dispatch({type: 'REPOSITORY_RECEIVED', data: {id: 1}});
        onGet({url: /issues*/, data: {...fakeIssue, milestone: {}, updated_at: '2019-05-13T14:30:23.406Z'}});

        const component = await asyncCreate(<Provider store={store}><Issue/></Provider>);

        expect(component.toJSON()).toMatchSnapshot();
    });

    it('renders the issue with no description', async () => {
        store.dispatch({type: 'REPOSITORY_RECEIVED', data: {id: 1}});
        onGet({url: /issues*/, data: {...fakeIssue, description: null}});

        const component = await asyncCreate(<Provider store={store}><Issue/></Provider>);

        expect(component.toJSON()).toMatchSnapshot();
    });

    it('renders the issue with some label', async () => {
        store.dispatch({type: 'REPOSITORY_RECEIVED', data: {id: 1}});
        onGet({url: /issues*/, data: {...fakeIssue, labels: ['some', 'label']}});

        const component = await asyncCreate(<Provider store={store}><Issue/></Provider>);

        expect(component.toJSON()).toMatchSnapshot();
    });

    it('shows the Close Issue button', async () => {
        store.dispatch({type: 'USER_RECEIVED', data: {id: 1}});
        store.dispatch({type: 'REPOSITORY_RECEIVED', data: {id: 1}});
        onGet({url: /issues*/, data: {...fakeIssue, state: 'opened', author: {id: 1}}});

        const component = mount(<Provider store={store}><Issue/></Provider>);
        await waitForAsync();
        component.update();

        const CloseIssue = getHeaderRightButton(store.getState());
        CloseIssue.props.onPress();

        expect(component.html()).toMatchSnapshot();
    });

    it('shows the Reopen button', async () => {
        store.dispatch({type: 'USER_RECEIVED', data: {id: 1}});
        store.dispatch({type: 'REPOSITORY_RECEIVED', data: {id: 1}});
        onGet({url: /issues*/, data: {...fakeIssue, state: 'closed', author: {id: 1}}});

        const component = mount(<Provider store={store}><Issue/></Provider>);
        await waitForAsync();
        component.update();

        const ReopenButton = getHeaderRightButton(store.getState());
        ReopenButton.props.onPress();

        expect(component.html()).toMatchSnapshot();
    });

    it('renders the issue with some assignee', async () => {
        store.dispatch({type: 'REPOSITORY_RECEIVED', data: {id: 1}});
        onGet({url: /issues*/, data: {...fakeIssue, assignees: [{name: 'someName1'}, {name: 'someName2'}]}});

        const component = await asyncCreate(<Provider store={store}><Issue/></Provider>);

        expect(component.toJSON()).toMatchSnapshot();
    });

    it('clicking on an assignee brings to the Profile page', async () => {
        store.dispatch({type: 'REPOSITORY_RECEIVED', data: {id: 1}});
        onGet({url: /issues*/, data: {...fakeIssue, assignees: [{id: 1, name: 'someName1'}]}});

        const component = mount(<Provider store={store}><Issue/></Provider>);
        await waitForAsync();
        component.update();

        const CircledImage = component.find('CircledImageComponent').at(1);
        CircledImage.props().onPress();
        component.update();

        expect(getRouteName(store.getState())).toEqual('Profile');
    });

    it('clicking on the author brings to the Profile page', async () => {
        store.dispatch({type: 'REPOSITORY_RECEIVED', data: {id: 1}});
        onGet({url: /issues*/, data: {...fakeIssue, assignees: [{id: 1, name: 'someName1'}]}});

        const component = mount(<Provider store={store}><Issue/></Provider>);
        await waitForAsync();
        component.update();

        const CircledImage = component.find('CircledImageComponent').at(0);
        CircledImage.props().onPress();
        component.update();

        expect(getRouteName(store.getState())).toEqual('Profile');
    });

    it('opens the comment section', async () => {
        store.dispatch({type: 'REPOSITORY_RECEIVED', data: {id: 1}});
        onGet({url: /issues*/, data: {...fakeIssue, state: 'opened'}});

        const component = mount(<Provider store={store}><Issue/></Provider>);
        await waitForAsync();
        component.update();

        const Footer = getFooter(store.getState());
        Footer.props.onPress();

        await waitForAsync();
        component.update();

        expect(component.html()).toMatchSnapshot();
    });

    xit('clicking the Comment button should call functions', async () => {
        store.dispatch({type: 'REPOSITORY_RECEIVED', data: {id: 1}});
        onGet({url: /issues*/, data: {...fakeIssue, state: 'opened'}});

        const component = mount(<Provider store={store}><Issue/></Provider>);
        await waitForAsync();
        component.update();

        // const MainButton = component.find('MainButton');
        // MainButton.props().onPress();

        await waitForAsync();
        component.update();

        expect(component.html()).toMatchSnapshot();
    });

    it('should get and display the discussions', async () => {
        store.dispatch({type: 'REPOSITORY_RECEIVED', data: {id: 1}});
        onGet({url: /discussions/, data: fakeDiscussions});
        onGet({url: /issues*/, data: {...fakeIssue, state: 'opened', web_url: ''}});

        const component = mount(<Provider store={store}><Issue/></Provider>);
        await waitForAsync();
        component.update();

        expect(component.html()).toMatchSnapshot();
    });

    it('should refresh when scrolling down the list', async () => {
        store.dispatch({type: 'REPOSITORY_RECEIVED', data: {id: 1}});
        onGet({url: /issues*/, data: {...fakeIssue, state: 'opened', web_url: ''}});

        const component = mount(<Provider store={store}><Issue/></Provider>);
        await waitForAsync();
        component.update();

        const ScrollableAppContainer = component.find('ScrollableAppContainerComponent').at(0);
        ScrollableAppContainer.props().refresh();
        await waitForAsync();
        component.update();

        expect(component.html()).toMatchSnapshot();
    });
});
