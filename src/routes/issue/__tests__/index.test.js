import {getRegisteredRoutes} from 'router';
import {getReducers} from 'store';

import * as index from '../index';

describe('Issue index', () => {
    it('should export Issue and reducers', () => {
        expect(JSON.stringify(index)).toEqual('{}');
        expect(getRegisteredRoutes().Issue).toBeDefined();
        expect(getReducers().issue).toBeDefined();
    });
});
